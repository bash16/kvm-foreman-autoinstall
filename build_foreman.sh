#!/bin/sh


# Check for root user
if [[ $EUID -ne 0 ]]; then
   echo -e "\e[31m This script must be run as root \e[0m" 
   exit 1
fi


# Check if bridged interfaces are up
MSG="Bridged interfaces are not operational, script exiting"

for i in br-mgmt br-pxe br-external; do

if [ "`cat /sys/class/net/$i/operstate`" != 'up' ]; then
echo -e "\e[31m $MSG \e[0m"; exit 1;

fi

done


# Add local repo to KVM host
cat > /etc/yum.repos.d/local.repo << __EOF__
[local-rhel7-os]
name=Local RHEL 7 OS
baseurl=http://openstack-rhelrepo-stg.itn.ftgroup/repo/rhel-7-server-rpms/
enabled=1
skip_if_unavailable=1
gpgcheck=0
__EOF__



# Create foreman VM
mkdir -p /vm/the_foreman
cp /root/foreman.qcow2 /vm/the_foreman/foreman.qcow2

# Import config files
for i in /root/foreman_config/ifcfg-*; do test -f "$i" && virt-customize -a /vm/the_foreman/foreman.qcow2 --upload $i:/etc/sysconfig/network-scripts; done

for i in /root/foreman_config/route-*; do test -f "$i" && virt-customize -a /vm/the_foreman/foreman.qcow2 --upload $i:/etc/sysconfig/network-scripts; done

[ -f "/root/foreman_config/resolv.conf" ] && virt-customize -a /vm/the_foreman/foreman.qcow2 --upload /root/foreman_config/resolv.conf:/etc/

virt-customize -a /vm/the_foreman/foreman.qcow2 --upload /root/deploy.sh:/root


# Install foreman VM
virt-install --import --boot hd -n the_foreman2 --description "the_foreman2" --os-type=Linux --os-variant=rhel7 --ram=8096 --vcpus=4 --disk path=/vm/the_foreman2/foreman.qcow2,bus=virtio,size=50,format=qcow2 --graphics none --network bridge:br-pxe --network bridge:br-mgmt --network bridge:br-external
