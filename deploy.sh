#!/usr/bin/bash

PROXY="10.114.27.31:3128"
VERSION_TAG="v10"


# Set proxy
export http_proxy="http://$PROXY"
export https_proxy="https://$PROXY"


# Add repos
cat > /etc/yum.repos.d/local.repo << __EOF__
[local-rhel7-os]
name=Local RHEL 7 OS
baseurl=http://openstack-rhelrepo-stg.itn.ftgroup/repo/rhel-7-server-rpms/
enabled=1
skip_if_unavailable=1
gpgcheck=0
__EOF__

cat > /etc/yum.repos.d/epel.repo << __EOF__
[local-epel-os]
name=Local EPEL 7 OS
baseurl=http://openstack-rhelrepo-stg.itn.ftgroup/repo/epel/
enabled=0
skip_if_unavailable=1
gpgcheck=0
__EOF__


cat > /etc/yum.repos.d/scl.repo << __EOF__
[local-rhel7-scl]
name=Local RHEL 7 SCL
baseurl=http://openstack-rhelrepo-stg.itn.ftgroup/repo/pulp-rhel7-scl/
enabled=1
skip_if_unavailable=1
gpgcheck=0
__EOF__

# Install packages
yum install -y git tmux

# Git config
git config --global http.sslverify false
git config --global credential.helper 'cache --timeout=36000'

# Clone release
cd /opt
git clone -b $VERSION_TAG https://openstack-git-stg.itn.ftgroup/deployment/ansible.git

# Install RPM requirements
cd /opt/ansible
yum install -y $(cat base-vm-yum-requirements.txt)

# Source environment & clone roles

(
cd /opt/ansible
. ./openstack-pyenv
)

(
cd /opt/ansible
. ./ansible-pyenv

ansible-playbook git_clone_requirements.yml
ansible-playbook clone.yml
)
